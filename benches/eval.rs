use criterion::{black_box, criterion_group, criterion_main, Criterion};
use rintf::{
    error::PrintfError, format_args::FormatArgProvider, format_options::FormatOptions,
    format_parser::FormatStringParser,
};

struct Args {}
impl FormatArgProvider for Args {
    type E = PrintfError;

    fn arg_i8(&self, _: usize) -> Result<i8, Self::E> {
        Ok(-8)
    }

    fn arg_i16(&self, _: usize) -> Result<i16, Self::E> {
        Ok(-16)
    }

    fn arg_i32(&self, _: usize) -> Result<i32, Self::E> {
        Ok(-32)
    }

    fn arg_u32(&self, _: usize) -> Result<u32, Self::E> {
        Ok(32)
    }

    fn arg_f32(&self, _: usize) -> Result<f32, Self::E> {
        Ok(3.2)
    }

    fn arg_str(&self, _: usize) -> Result<&str, Self::E> {
        Ok("thirty two")
    }

    fn arg_u8(&self, _: usize) -> Result<u8, Self::E> {
        Ok(b'z')
    }

    fn get_argc_offset(&self) -> usize {
        0
    }

    fn format<W: std::fmt::Write>(
        &self,
        _extension: char,
        _options: &FormatOptions,
        _argc_offset: usize,
        _fmt: &mut W,
    ) -> Result<(), Self::E> {
        todo!()
    }
}

fn bench_parse(c: &mut Criterion) {
    let parser = FormatStringParser::default();
    let format_string = "%s %f";
    let parsed = parser.parse(format_string).unwrap();
    let mut out = String::new();
    let args = Args {};
    c.bench_function("FormatStringResult::evaluate", |b| {
        b.iter(|| {
            out.clear();
            parsed.evaluate(&args, black_box(&mut out)).unwrap();
        })
    });
}

criterion_group!(benches, bench_parse);
criterion_main!(benches);
