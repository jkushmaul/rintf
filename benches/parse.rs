use criterion::{black_box, criterion_group, criterion_main, Criterion};
use rintf::format_parser::FormatStringParser;

fn bench_parse(c: &mut Criterion) {
    let parser = FormatStringParser::default();
    let format_string = "%s %f";
    c.bench_function("FormatStringParser::parse", |b| {
        b.iter(|| {
            parser.parse(black_box(format_string)).unwrap();
        })
    });
}

criterion_group!(benches, bench_parse);
criterion_main!(benches);
