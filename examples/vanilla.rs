use std::num::{ParseFloatError, ParseIntError};

use clap::Parser;
use rintf::{
    error::PrintfError, format_args::FormatArgProvider, format_options::FormatOptions,
    format_parser::FormatStringParser,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ArgError {
    #[error("Mising argument {0}")]
    MissingArg(usize),
    #[error(transparent)]
    PrintfError(#[from] PrintfError),
    #[error(transparent)]
    FmtError(#[from] std::fmt::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error(transparent)]
    ParseFloatError(#[from] ParseFloatError),
}

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    format: String,
    args: Vec<String>,
}

impl Args {
    fn get_arg(&self, arg_index: usize) -> Result<&str, ArgError> {
        if arg_index > 0 {
            if let Some(s) = self.args.get(arg_index - 1) {
                return Ok(s);
            }
        }

        return Err(ArgError::MissingArg(arg_index));
    }
}

impl FormatArgProvider for Args {
    type E = ArgError;

    fn arg_i8(&self, arg_index: usize) -> Result<i8, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn arg_i16(&self, arg_index: usize) -> Result<i16, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn arg_i32(&self, arg_index: usize) -> Result<i32, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn arg_u32(&self, arg_index: usize) -> Result<u32, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn arg_f32(&self, arg_index: usize) -> Result<f32, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn arg_str(&self, arg_index: usize) -> Result<&str, Self::E> {
        self.get_arg(arg_index)
    }

    fn arg_u8(&self, arg_index: usize) -> Result<u8, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn get_argc_offset(&self) -> usize {
        0
    }

    fn format<W: std::fmt::Write>(
        &self,
        _extension: char,
        _options: &FormatOptions,
        _argc_offset: usize,
        _fmt: &mut W,
    ) -> Result<(), Self::E> {
        todo!()
    }
}

/// cargo run --example=vanilla -- --format 'a string: %s, an int: %d, a float: %f' test "124" "42.24"
/// Finished `dev` profile [unoptimized + debuginfo] target(s) in 0.04s
/// Running `target\debug\examples\vanilla.exe --format 'a string: %s, an int: %d, a float: %f' test 124 42.24`
///     Evaluated a string: %s, an int: %d, a float: %f:
///         a string: test, an int: 124, a float: 42.24
pub fn main() {
    let args = match Args::try_parse() {
        Ok(a) => a,
        Err(_) => Args {
            format: "%f".into(),
            args: vec!["1.2".into()],
        },
    };

    let parser = FormatStringParser::default();
    let result = parser
        .parse(&args.format)
        .map_err(|e| panic!("Could not parse {}: {}", &args.format, e))
        .unwrap();

    let mut out = String::new();
    result
        .evaluate(&args, &mut out)
        .map_err(|e| panic!("Could not evaluate: {}", e))
        .unwrap();

    println!("Evaluated format string '{}'", &args.format);
    println!("  Result: '{}'", out);
}
