use crate::format_parser::{self, FormatStringState, FormatStringType};
use nom::{branch::alt, bytes::complete::tag, combinator::map, sequence::preceded};
use std::fmt::{Display, Formatter};

/// Prefixed with a dot, followed by precision amount or * for argument provided
/// eg)
///     .4 for a precision of '4'
///     .* to use precision on argument list
///         Which - I need to add
#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub struct Precision {
    precision: Option<usize>,
    is_arg: bool,
}

impl Precision {
    pub fn is_arg(&self) -> bool {
        self.is_arg
    }

    pub fn get(&self) -> &Option<usize> {
        &self.precision
    }

    pub fn from_arg() -> Self {
        Self {
            is_arg: true,
            precision: None,
        }
    }
    pub fn from_precision(precision: usize) -> Self {
        Self {
            is_arg: false,
            precision: Some(precision),
        }
    }
}

impl Display for Precision {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.is_arg {
            return f.write_str(".*");
        }

        if let Some(precision) = self.precision {
            return f.write_fmt(format_args!(".{}", precision));
        }

        Ok(())
    }
}

impl FormatStringType for Precision {
    fn parse<'a>(input: FormatStringState<'a>) -> nom::IResult<FormatStringState<'a>, Self>
    where
        Self: Sized,
    {
        preceded(
            tag("."),
            alt((
                map(tag("*"), |_| Precision::from_arg()),
                map(format_parser::parse_usize, |r| Precision::from_precision(r)),
            )),
        )(input)
    }
}

#[cfg(test)]
mod test {
    use super::Precision;
    use crate::format_parser::{FormatStringParser, FormatStringState, FormatStringType};

    fn get_test_cases() -> Vec<(&'static str, &'static str, Precision)> {
        vec![
            ("specified", ".4", Precision::from_precision(4)),
            ("arg", ".*", Precision::from_arg()),
        ]
    }

    #[test]
    fn test_display() {
        let mut test_cases = get_test_cases();
        test_cases.push(("Default", "", Precision::default()));

        for (name, format_string, object) in test_cases {
            let actual = format!("{}", object);
            assert_eq!(
                actual, format_string,
                "Case[{}] expected display string '{}' from {:?}\n   Got '{}'",
                name, format_string, object, actual
            );
        }
    }

    #[test]
    fn test_parse() {
        let test_cases = get_test_cases();
        let parser = FormatStringParser::default();

        for (name, format_string, object) in test_cases {
            let input: FormatStringState = parser.state(&format_string);

            let (leftover, actual) = Precision::parse(input).unwrap();
            assert_eq!(
                leftover.input, "",
                "Case[{}] leftover found: '{}' from input '{}'",
                name, leftover.input, format_string
            );
            assert_eq!(
                actual, object,
                "Case[{}] failed to parse '{}' into {:?}\n  Got {:?}",
                name, format_string, object, actual
            );
        }
    }
}
