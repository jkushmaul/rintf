mod format_flags;
mod length_modifier;
mod precision;
mod width;

use std::fmt::{Debug, Display};

pub use format_flags::{ArgFormatFlag, ArgFormatFlags};
pub use length_modifier::*;
use nom::{bytes::complete::tag, combinator::opt, IResult};
pub use precision::*;
pub use width::*;

use crate::format_parser::{parse_usize, FormatStringState, FormatStringType};

#[derive(Default, Clone, Debug, PartialEq)]
pub struct FormatOptions {
    pub index: Option<usize>,  // Posix Ext.  Might can this one...
    pub flags: ArgFormatFlags, // can be 0 or more, but should be unique
    pub width: Width,
    pub precision: Precision,
    pub arg_size: Option<LengthModifier>,
}

impl FormatStringType for FormatOptions {
    fn parse<'a>(input: FormatStringState<'a>) -> nom::IResult<FormatStringState<'a>, Self>
    where
        Self: Sized,
    {
        let (input, index) = opt(parse_format_parameter)(input)?;
        let (input, flags) = opt(ArgFormatFlags::parse)(input)?;
        let (input, width) = opt(Width::parse)(input)?;
        let (input, precision) = opt(Precision::parse)(input)?;
        let (input, arg_size) = opt(LengthModifier::parse)(input)?;

        let flags = flags.unwrap_or_default();
        let width = width.unwrap_or_default();
        let precision = precision.unwrap_or_default();

        Ok((
            input,
            Self {
                index,
                flags,
                width,
                precision,
                arg_size,
            },
        ))
    }
}

fn parse_format_parameter<'a>(
    input: FormatStringState<'a>,
) -> IResult<FormatStringState<'a>, usize> {
    let (input, s) = nom::sequence::terminated(parse_usize, tag("$"))(input)?;

    Ok((input, s))
}

// %[options]conversion
// -> [$][flags][width][.precision][length modifier]
impl Display for FormatOptions {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        //$?
        if let Some(field) = self.index {
            f.write_fmt(format_args!("{}$", field))?;
        }
        std::fmt::Display::fmt(&self.flags, f)?;
        std::fmt::Display::fmt(&self.width, f)?;
        std::fmt::Display::fmt(&self.precision, f)?;
        if let Some(arg_size) = self.arg_size {
            std::fmt::Display::fmt(&arg_size, f)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::{ArgFormatFlag, ArgFormatFlags, FormatOptions, LengthModifier, Precision, Width};
    use crate::format_parser::{FormatStringParser, FormatStringState, FormatStringType};

    // Name, string, option
    fn get_test_cases() -> Vec<(&'static str, &'static str, FormatOptions)> {
        vec![
            ("default", "", FormatOptions::default()),
            (
                "parameter field",
                "4$",
                FormatOptions {
                    index: Some(4),
                    ..Default::default()
                },
            ),
            (
                "flags field",
                "-+",
                FormatOptions {
                    flags: ArgFormatFlags::from(
                        ArgFormatFlag::LeftAlign | ArgFormatFlag::MarkPositive,
                    ),
                    ..Default::default()
                },
            ),
            (
                "width 4",
                "4",
                FormatOptions {
                    width: Width::from_width(4),
                    ..Default::default()
                },
            ),
            (
                "width arg",
                "*",
                FormatOptions {
                    width: Width::from_arg(),
                    ..Default::default()
                },
            ),
            (
                "precision 4",
                ".4",
                FormatOptions {
                    precision: Precision::from_precision(4),
                    ..Default::default()
                },
            ),
            (
                "precision from arg",
                ".*",
                FormatOptions {
                    precision: Precision::from_arg(),
                    ..Default::default()
                },
            ),
            (
                "length modifier",
                "l",
                FormatOptions {
                    arg_size: Some(LengthModifier::I32),
                    ..Default::default()
                },
            ),
            (
                "all options",
                "4$-054.5ll",
                FormatOptions {
                    index: Some(4),
                    flags: ArgFormatFlags::from(ArgFormatFlag::LeftAlign | ArgFormatFlag::Zero),
                    width: Width::from_width(54),
                    precision: Precision::from_precision(5),
                    arg_size: Some(LengthModifier::I64),
                },
            ),
        ]
    }

    #[test]
    fn test_display() {
        //Should build the options string from after the %, up to the conversion specifier
        let test_cases = get_test_cases();
        for (name, format_string, object) in test_cases {
            let actual = format!("{}", object);
            assert_eq!(
                actual, format_string,
                "Case[{}] failed to display to string '{}' from {:?}\n   Got '{}'",
                name, format_string, object, actual
            );
        }
    }

    #[test]
    fn test_parse() {
        // test a format specifier - from after the %, up to the conversion specifier
        let test_cases = get_test_cases();
        let parser = FormatStringParser::default();

        for (name, format_string, object) in test_cases {
            let input: FormatStringState = parser.state(format_string);

            let (leftover, actual) = FormatOptions::parse(input).unwrap();
            assert_eq!(
                leftover.input, "",
                "Case[{}] leftover found: '{}' from input '{}'",
                name, leftover.input, format_string
            );
            assert_eq!(
                actual, object,
                "Case[{}] failed to parse '{}' into {:?}\n  Got {:?}",
                name, format_string, object, actual
            );
        }
    }
}
