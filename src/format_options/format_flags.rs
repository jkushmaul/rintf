use crate::format_parser::{FormatStringState, FormatStringType};
use flagset::{flags, FlagSet};
use nom::{character::complete::one_of, combinator::map_res, multi::fold_many0};
use std::fmt::{Display, Formatter};

flags! {
    ///  Flag characters
    ///  The character % is followed by zero or more of these flags

    pub enum ArgFormatFlag : u8 {
        /// `-` The converted value is to be left adjusted on the field
        /// boundary.  (The default is right justification.)  The
        /// converted value is padded on the right with blanks, rather
        /// than on the left with blanks or zeros.  A - overrides a 0
        /// if both are given.
        LeftAlign,
        /// `+`  A sign (+ or -) should always be placed before a number
        /// produced by a signed conversion.  By default, a sign is
        /// used only for negative numbers.  A + overrides a space if
        /// both are used.
        MarkPositive,
        /// ` `(a space) A blank should be left before a positive number
        /// (or empty string) produced by a signed conversion.
        Space,
        /// `0` The value should be zero padded.  For d, i, o, u, x, X, a,
        /// A, e, E, f, F, g, and G conversions, the converted value
        /// is padded on the left with zeros rather than blanks.  If
        /// the 0 and - flags both appear, the 0 flag is ignored.  If
        /// a precision is given with an integer conversion (d, i, o,
        /// u, x, and X), the 0 flag is ignored.  For other
        /// conversions, the behavior is undefined.
        Zero,
        /// For decimal conversion (i, d, u, f, F, g, G) the output is
        /// to be grouped with thousands' grouping characters if the
        /// locale information indicates any.  (See setlocale(3).)
        /// Note that many versions of gcc(1) cannot parse this option
        /// and will issue a warning.  (SUSv2 did not include %'F, but
        /// SUSv3 added it.)  Note also that the default locale of a C
        /// program is "C" whose locale information indicates no
        /// thousands' grouping character.  Therefore, without a prior
        /// call to setlocale(3), no thousands' grouping characters
        /// will be printed.
        Thousands,
        /// `#` The value should be converted to an "alternate form".  For
        /// o conversions, the first character of the output string is
        /// made zero (by prefixing a 0 if it was not zero already).
        /// For x and X conversions, a nonzero result has the string
        /// "0x" (or "0X" for X conversions) prepended to it.  For a,
        /// A, e, E, f, F, g, and G conversions, the result will
        /// always contain a decimal point, even if no digits follow
        /// it (normally, a decimal point appears in the results of
        /// those conversions only if a digit follows).  For g and G
        /// conversions, trailing zeros are not removed from the
        /// result as they would otherwise be.  For m, if errno
        /// contains a valid error code, the output of
        /// strerrorname_np(errno) is printed; otherwise, the value
        /// stored in errno is printed as a decimal number.  For other
        /// conversions, the result is undefined.
        AlternateForm,

        /// `I``      For decimal integer conversion (i, d, u) the output uses
        /// the locale's alternative output digits, if any.  For
        /// example, since glibc 2.2.3 this will give Arabic-Indic
        /// digits in the Persian ("fa_IR") locale.
        Glibc,
    }
}

impl TryFrom<u8> for ArgFormatFlag {
    type Error = strum::ParseError;

    fn try_from(c: u8) -> Result<Self, Self::Error> {
        let c = if c == b'-' {
            Self::LeftAlign
        } else if c == b'+' {
            Self::MarkPositive
        } else if c == b' ' {
            Self::Space
        } else if c == b'0' {
            Self::Zero
        } else if c == b'\'' {
            Self::Thousands
        } else if c == b'#' {
            Self::AlternateForm
        } else if c == b'I' {
            Self::Glibc
        } else {
            return Err(strum::ParseError::VariantNotFound);
        };

        Ok(c)
    }
}

impl Display for ArgFormatFlag {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Self::LeftAlign => "-",
            Self::MarkPositive => "+",
            Self::Space => " ",
            Self::Zero => "0",
            Self::Thousands => "'",
            Self::AlternateForm => "#",
            Self::Glibc => "I",
        };
        f.write_fmt(format_args!("{}", s))
    }
}

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub struct ArgFormatFlags {
    pub flags: FlagSet<ArgFormatFlag>,
}

impl From<FlagSet<ArgFormatFlag>> for ArgFormatFlags {
    fn from(flags: FlagSet<ArgFormatFlag>) -> Self {
        Self { flags }
    }
}
impl From<ArgFormatFlag> for ArgFormatFlags {
    fn from(value: ArgFormatFlag) -> Self {
        Self {
            flags: FlagSet::default() | value,
        }
    }
}

impl Display for ArgFormatFlags {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for flag in self.flags {
            f.write_fmt(format_args!("{}", flag))?;
        }
        Ok(())
    }
}

impl FormatStringType for ArgFormatFlags {
    fn parse<'a>(input: FormatStringState<'a>) -> nom::IResult<FormatStringState<'a>, Self>
    where
        Self: Sized,
    {
        let (l, flags) = fold_many0(
            map_res(one_of("-+ 0'#I"), |c| ArgFormatFlag::try_from(c as u8)),
            || FlagSet::<ArgFormatFlag>::default(),
            |mut acc: FlagSet<ArgFormatFlag>, item: ArgFormatFlag| {
                acc |= item;
                acc
            },
        )(input)?;

        Ok((l, ArgFormatFlags { flags }))
    }
}

#[cfg(test)]
mod test {
    use super::{ArgFormatFlag, ArgFormatFlags};
    use crate::format_parser::{FormatStringParser, FormatStringState, FormatStringType};

    #[test]
    fn test_display() {
        let test_cases = vec![
            ("-", ArgFormatFlags::from(ArgFormatFlag::LeftAlign)),
            ("+", ArgFormatFlags::from(ArgFormatFlag::MarkPositive)),
            (" ", ArgFormatFlags::from(ArgFormatFlag::Space)),
            ("0", ArgFormatFlags::from(ArgFormatFlag::Zero)),
            ("'", ArgFormatFlags::from(ArgFormatFlag::Thousands)),
            ("#", ArgFormatFlags::from(ArgFormatFlag::AlternateForm)),
            ("I", ArgFormatFlags::from(ArgFormatFlag::Glibc)),
        ];

        for (expected, input) in test_cases {
            let actual = format!("{}", input);
            assert_eq!(actual, expected);
        }
    }

    #[test]
    fn test_parse_format_flags() {
        let input = "-+ 0'#I_leftover";

        let parser = FormatStringParser::default();
        let input: FormatStringState = parser.state(input);

        let (leftover, actual) = ArgFormatFlags::parse(input).unwrap();

        let expected = ArgFormatFlag::LeftAlign
            | ArgFormatFlag::MarkPositive
            | ArgFormatFlag::Space
            | ArgFormatFlag::Zero
            | ArgFormatFlag::Thousands
            | ArgFormatFlag::AlternateForm
            | ArgFormatFlag::Glibc;

        assert_eq!(leftover.input, "_leftover");
        assert_eq!(actual.flags, expected);
    }
}
