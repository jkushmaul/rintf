use crate::format_parser::{self, FormatStringState, FormatStringType};
use nom::{branch::alt, bytes::complete::tag, combinator::map, IResult};
use std::fmt::{Display, Formatter};

#[derive(Default, std::fmt::Debug, Clone, Copy, PartialEq)]
pub struct Width {
    width: Option<usize>,
    is_arg: bool,
}

impl Width {
    pub fn is_arg(&self) -> bool {
        self.width.is_none()
    }

    pub fn get(&self) -> &Option<usize> {
        &self.width
    }
    pub fn from_arg() -> Self {
        Self {
            is_arg: true,
            width: None,
        }
    }
    pub fn from_width(width: usize) -> Self {
        Self {
            is_arg: false,
            width: Some(width),
        }
    }
}

impl Display for Width {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.is_arg {
            return f.write_str("*");
        }

        if let Some(width) = self.width {
            return f.write_fmt(format_args!("{}", width));
        }

        Ok(())
    }
}

impl FormatStringType for Width {
    fn parse<'a>(input: FormatStringState<'a>) -> IResult<FormatStringState<'a>, Self>
    where
        Self: Sized,
    {
        alt((
            map(tag("*"), |_| Width::from_arg()),
            map(format_parser::parse_usize, |r| Width::from_width(r)),
        ))(input)
    }
}

#[cfg(test)]
mod tests {
    use super::Width;
    use crate::format_parser::{FormatStringParser, FormatStringState, FormatStringType};

    fn get_test_cases() -> Vec<(&'static str, &'static str, Width)> {
        vec![
            ("specified", "4", Width::from_width(4)),
            ("arg", "*", Width::from_arg()),
        ]
    }

    #[test]
    fn test_display() {
        let mut test_cases = get_test_cases();
        test_cases.push(("Default", "", Width::default()));
        for (name, format_string, object) in test_cases {
            let actual = format!("{}", object);
            assert_eq!(
                actual, format_string,
                "Case[{}] expected display string '{}' from {:?}\n   Got '{}'",
                name, format_string, object, actual
            );
        }
    }

    #[test]
    fn test_parse() {
        let test_cases = get_test_cases();
        let parser = FormatStringParser::default();

        for (name, format_string, object) in test_cases {
            let input: FormatStringState = parser.state(format_string);
            let (leftover, actual) = Width::parse(input).unwrap();
            assert_eq!(
                leftover.input, "",
                "Case[{}] leftover found: '{}' from input '{}'",
                name, leftover.input, format_string
            );
            assert_eq!(
                actual, object,
                "Case[{}] failed to parse '{}' into {:?}\n  Got {:?}",
                name, format_string, object, actual
            );
        }
    }
}
