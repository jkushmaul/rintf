use crate::{
    error::PrintfError,
    format_args::ExpectedArgType,
    format_parser::{FormatStringState, FormatStringType},
};
use nom::{branch::alt, bytes::complete::tag, combinator::map_res};
use std::str::FromStr;

/// Length modifier
/// Here, "integer conversion" stands for d, i, o, u, x, or X
/// conversion.
///
/// Here for documentation purposes and unsupported at this time
#[derive(Debug, Clone, Copy, Eq, PartialEq, strum::Display, strum::EnumString)]
#[strum(serialize_all = "lowercase")]
pub enum LengthModifier {
    /// hh: expect
    ///  A following integer conversion corresponds to a signed
    ///          char or unsigned char argument, or a following n
    ///          conversion corresponds to a pointer to a signed char
    ///          argument.
    #[strum(serialize = "hh")]
    I32FromChar,

    /// h: expect int sized promoted from a short.
    ///
    /// A following integer conversion corresponds to a short or
    /// unsigned short argument, or a following n conversion
    /// corresponds to a pointer to a short argument.
    #[strum(serialize = "h")]
    I32FromShort,

    /// ll: expect long-long arg
    /// (ell-ell).  A following integer conversion corresponds to
    /// a long long or unsigned long long argument, or a following
    /// n conversion corresponds to a pointer to a long long
    /// argument.
    #[strum(serialize = "ll")]
    I64,

    /// L: expect long double
    /// A following a, A, e, E, f, F, g, or G conversion
    /// corresponds to a long double argument.  (C99 allows %LF,
    /// but SUSv2 does not.)
    #[strum(serialize = "L")]
    F64,

    /// l: expect long sized arg
    /// (ell) A following integer conversion corresponds to a long
    /// or unsigned long argument, or a following n conversion
    /// corresponds to a pointer to a long argument, or a
    /// following c conversion corresponds to a wint_t argument,
    /// or a following s conversion corresponds to a pointer to
    /// wchar_t argument.  On a following a, A, e, E, f, F, g, or
    /// G conversion, this length modifier is ignored (C99; not in
    /// SUSv2).
    #[strum(serialize = "l")]
    I32,

    /// z: expect size_t arg
    /// A following integer conversion corresponds to a size_t or
    /// ssize_t argument, or a following n conversion corresponds
    /// to a pointer to a size_t argument.
    #[strum(serialize = "z")]
    SizeT,

    /// j: intmax_t arg
    /// A following integer conversion corresponds to an intmax_t
    /// or uintmax_t argument, or a following n conversion
    /// corresponds to a pointer to an intmax_t argument.
    #[strum(serialize = "j")]
    IntMaxT,

    /// t: ptrdiff_t arg
    ///A following integer conversion corresponds to a ptrdiff_t
    ///           argument, or a following n conversion corresponds to a
    ///              pointer to a ptrdiff_t argument.
    #[strum(serialize = "t")]
    PtrDiffT,
}

impl TryInto<ExpectedArgType> for LengthModifier {
    type Error = PrintfError;

    fn try_into(self) -> Result<ExpectedArgType, Self::Error> {
        let sized = match self {
            LengthModifier::I32FromChar => ExpectedArgType::I8,
            LengthModifier::I32FromShort => ExpectedArgType::I16,
            LengthModifier::I32 => ExpectedArgType::I32,
            _ => return Err(PrintfError::NotSupported("arg size".into())),
        };
        return Ok(sized);
    }
}

impl FormatStringType for LengthModifier {
    fn parse<'a>(input: FormatStringState<'a>) -> nom::IResult<FormatStringState<'a>, Self>
    where
        Self: Sized,
    {
        let (input, res) = map_res(
            alt((
                tag("hh"),
                tag("h"),
                tag("ll"),
                tag("L"),
                tag("l"),
                tag("z"),
                tag("j"),
                tag("t"),
            )),
            |input: FormatStringState<'a>| LengthModifier::from_str(&input.input),
        )(input)?;

        Ok((input, res))
    }
}

#[cfg(test)]
mod tests {
    use super::LengthModifier;
    use crate::format_parser::{FormatStringParser, FormatStringState, FormatStringType};

    #[test]
    fn test_parse_arg_sizes() {
        let test_cases = &[
            ("hh", LengthModifier::I32FromChar),
            ("h", LengthModifier::I32FromShort),
            ("ll", LengthModifier::I64),
            ("L", LengthModifier::F64),
            ("l", LengthModifier::I32),
            ("z", LengthModifier::SizeT),
            ("j", LengthModifier::IntMaxT),
            ("t", LengthModifier::PtrDiffT),
        ];
        let parser = FormatStringParser::default();
        for (input, expected) in test_cases {
            let input = format!("{} leftover", input);

            let input: FormatStringState = parser.state(&input);

            let (leftover, r) = LengthModifier::parse(input).unwrap();
            assert_eq!(leftover.input, " leftover");
            assert_eq!(r, *expected);
        }
    }

    #[test]
    pub fn test_display() {
        let test_cases = &[
            ("hh", LengthModifier::I32FromChar),
            ("h", LengthModifier::I32FromShort),
            ("ll", LengthModifier::I64),
            ("l", LengthModifier::I32),
            ("L", LengthModifier::F64),
            ("z", LengthModifier::SizeT),
            ("z", LengthModifier::SizeT),
            ("j", LengthModifier::IntMaxT),
            ("t", LengthModifier::PtrDiffT),
        ];

        for (expected, input) in test_cases {
            let actual = format!("{}", input);
            assert_eq!(actual, *expected);
        }
    }
}
