use crate::{
    format_args::{ExpectedArgType, FormatArgProvider},
    format_parser::{self, FormatStringState, FormatStringType},
    format_specifier::{FormatSpecifier, SpecifierType},
};
use nom::{branch::alt, IResult};

/// The building blocks of a format string
#[derive(Clone, Debug, PartialEq)]
pub enum FormatFragment {
    /// A literal string
    Literal(String),
    /// A type with options
    FormatSpecifier(FormatSpecifier),
}

impl FormatFragment {
    pub(crate) fn format<W: std::fmt::Write, A: FormatArgProvider>(
        &self,
        args: &A,
        argc_offset: &mut usize,
        fmt: &mut W,
    ) -> Result<(), A::E> {
        match self {
            FormatFragment::Literal(s) => return Ok(fmt.write_str(&s)?),

            FormatFragment::FormatSpecifier(f) => match f.arg_type {
                SpecifierType::Builtin(b) => {
                    let expected_arg_type: ExpectedArgType = match f.options.arg_size {
                        // It isn't supported yet anyways but that's thrown inside try_into
                        Some(sized) => sized.try_into()?,
                        None => b.try_into()?,
                    };

                    let arg_value = match expected_arg_type.get_arg(&f.options, *argc_offset, args)
                    {
                        Ok(v) => v,
                        Err(e) => {
                            eprintln!("Error: {}", e);
                            return Err(e)?;
                        }
                    };
                    if let Err(e) = arg_value.format(&f.options, fmt) {
                        eprintln!("Error: {}", e);
                        return Err(e)?;
                    }

                    *argc_offset += 1;
                }
                SpecifierType::Extension(c) => {
                    args.format(c, &f.options, *argc_offset, fmt)?;
                    *argc_offset += 1;
                }
            },
        };

        Ok(())
    }
}

impl FormatStringType for FormatFragment {
    fn parse<'a>(input: FormatStringState<'a>) -> nom::IResult<FormatStringState<'a>, Self>
    where
        Self: Sized,
    {
        alt((literal_fragment, format_specifier))(input)
    }
}

fn format_specifier<'a>(
    input: FormatStringState<'a>,
) -> IResult<FormatStringState<'a>, FormatFragment> {
    let (input, res) = FormatSpecifier::parse(input)?;

    Ok((input, FormatFragment::FormatSpecifier(res)))
}

fn literal_fragment<'a>(
    input: FormatStringState<'a>,
) -> IResult<FormatStringState<'a>, FormatFragment> {
    let (input, res) = format_parser::parse_string_literal(input)?;
    let res = FormatFragment::Literal(res.to_owned());

    Ok((input, res))
}

#[cfg(test)]
mod tests {
    use super::literal_fragment;
    use crate::{format_parser::FormatStringParser, format_string::FormatFragment};

    #[test]
    fn test_string_literal_bare() {
        let input = "this is a string";

        let parser = FormatStringParser::default();
        let state = parser.state(input);

        let (leftover, result) = literal_fragment(state).unwrap();

        assert_eq!(leftover.input, "");
        assert_eq!(result, FormatFragment::Literal(input.into()));
    }

    #[test]
    fn test_string_literal_leftover() {
        let input = "this is a string%butnotthis";

        let parser = FormatStringParser::default();
        let state = parser.state(input);

        let (leftover, result) = literal_fragment(state).unwrap();

        assert_eq!(leftover.input, "%butnotthis");
        assert_eq!(result, FormatFragment::Literal("this is a string".into()));
    }
}
