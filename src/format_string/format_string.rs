use crate::{
    error::NomParsingError,
    format_args::FormatArgProvider,
    format_parser::{FormatStringState, FormatStringType},
};
use nom::{combinator::map_res, multi::many0, IResult};
use std::fmt::Write;

use super::FormatFragment;

#[derive(Debug, PartialEq)]
pub struct FormatStringResult {
    pub fragments: Vec<FormatFragment>,
}

impl FormatStringType for FormatStringResult {
    fn parse<'b>(input: FormatStringState<'b>) -> IResult<FormatStringState<'b>, Self>
    where
        Self: Sized,
    {
        //cultural learnings for make nom great, many0 should probably only be used where you want to match the very end of the string with complete whole matches or it errs.
        //Such as the case in this specific parsing function.  I don't want anything else, or an error.

        //let res: Result<(&'a str, Vec<Vec<FormatFragment>>), nom::Err<nom::error::Error<&'a str>>> =
        // expected `parse` to be a fn item that returns `Result<(&ArgTypeInput<'_>, _), Err<_>>`, but it returns `Result<(ArgTypeInput<'_>, Vec<FormatFragment>), Err<Error<ArgTypeInput<'_>>>>`

        let (input, fragments) = map_res(
            many0(FormatFragment::parse),
            |fragments: Vec<FormatFragment>| transform_format_fragments(fragments),
        )(input)?;

        Ok((input, FormatStringResult { fragments }))
    }
}

fn transform_format_fragments<'a>(
    before: Vec<FormatFragment>,
) -> Result<Vec<FormatFragment>, NomParsingError<FormatStringState<'a>>> {
    let mut v = vec![];

    let mut need_args = false;
    let mut arg_id = 0;
    let mut index = 0;
    for b in before {
        index = index + 1;
        match b {
            FormatFragment::Literal(s) => {
                if let Some(FormatFragment::Literal(last)) = v.last_mut() {
                    //Last was literal, and so is this. Just add to that one.
                    last.push_str(&s);
                } else {
                    v.push(FormatFragment::Literal(s));
                }
                continue;
            }

            FormatFragment::FormatSpecifier(mut f) => {
                arg_id += 1;

                match f.options.index {
                    Some(i) => {
                        if !need_args && arg_id != 1 {
                            // It's starting the parameter positions but there were earlier formatters that did not.
                            return Err(NomParsingError::PositionsNotAllowed(index));
                        } else if !need_args {
                            need_args = true;
                        }
                        f.options.index = Some(i);
                    }
                    None => {
                        if need_args {
                            //Earlier formatters specified position tbut this did not
                            return Err(NomParsingError::ContinuedPositionsRequired(index));
                        }
                    }
                };

                v.push(FormatFragment::FormatSpecifier(f));
            }
        };
    }
    Ok(v)
}

impl FormatStringResult {
    pub fn evaluate<W: Write, A: FormatArgProvider>(
        &self,
        args: &A,
        fmt: &mut W,
    ) -> Result<(), A::E> {
        let mut argc_offset = args.get_argc_offset();

        //Run through each fragment and build a string
        for f in &self.fragments {
            f.format(args, &mut argc_offset, fmt)?;
        }

        Ok(())
    }
}

#[cfg(test)]
pub mod test {
    use crate::{
        format_args::StringArgvProvider,
        format_options::{FormatOptions, Precision, Width},
        format_parser::{FormatStringParser, FormatStringState, FormatStringType},
        format_specifier::{ConversionSpecifier, FormatSpecifier, SpecifierType},
        format_string::{FormatFragment, FormatStringResult},
        tests::assert_list_equals,
    };

    #[test]
    fn test_arg_positions() {
        let input = "%i %*d %.*f %c";

        let parser = FormatStringParser::default();
        let state = parser.state(input);

        let (leftover, actual) = FormatStringResult::parse(state).unwrap();
        assert_eq!(
            leftover.input, "",
            "expected no leftover\nbut got '{}'\n with object: {:#?}",
            leftover.input, actual
        );
        let literal_space = FormatFragment::Literal(" ".into());
        let expected = vec![
            FormatFragment::FormatSpecifier(FormatSpecifier {
                arg_type: SpecifierType::Builtin(ConversionSpecifier::decimalI32_i),
                options: Default::default(),
            }),
            literal_space.clone(),
            FormatFragment::FormatSpecifier(FormatSpecifier {
                arg_type: SpecifierType::Builtin(ConversionSpecifier::decimalI32_d),
                options: FormatOptions {
                    width: Width::from_arg(),
                    ..Default::default()
                },
            }),
            literal_space.clone(),
            FormatFragment::FormatSpecifier(FormatSpecifier {
                arg_type: SpecifierType::Builtin(ConversionSpecifier::float),
                options: FormatOptions {
                    precision: Precision::from_arg(),
                    ..Default::default()
                },
            }),
            literal_space,
            FormatFragment::FormatSpecifier(FormatSpecifier {
                arg_type: SpecifierType::Builtin(ConversionSpecifier::char),
                options: FormatOptions::default(),
            }),
        ];
        assert_list_equals("arg_positions", &actual.fragments, &expected);
    }

    #[test]
    fn test_odd_case() {
        // let arg_types = ConversionSpecifier::build_format_extensions();
        let input = "alias %1$s \"%2$s %1$s ${* ?}\"\n";
        //arg[1]: "bufstr_get"
        //arg[2]: "qc_cmd_svcl"
        //arg[3]: ""
        //  ->  Xonotic: "alias bufstr_get \"qc_cmd_svcl bufstr_get ${* ?}\"\n"

        let parser = FormatStringParser::default();
        let input = parser.state(input);

        let (leftover, actual) = match FormatStringResult::parse(input) {
            Ok(f) => f,
            Err(e) => panic!("Could not parse format string '{}': {}", input.input, e),
        };
        assert_eq!(leftover.input, "");

        let expected = FormatStringResult {
            fragments: vec![
                FormatFragment::Literal(String::from("alias ")),
                FormatFragment::FormatSpecifier(FormatSpecifier {
                    options: FormatOptions {
                        index: Some(1),
                        flags: Default::default(),
                        width: Default::default(),
                        precision: Default::default(),
                        arg_size: None,
                    },
                    arg_type: SpecifierType::Builtin(ConversionSpecifier::string),
                }),
                FormatFragment::Literal(String::from(" \"")),
                FormatFragment::FormatSpecifier(FormatSpecifier {
                    options: FormatOptions {
                        index: Some(2),
                        flags: Default::default(),
                        width: Default::default(),
                        precision: Default::default(),
                        arg_size: None,
                    },
                    arg_type: SpecifierType::Builtin(ConversionSpecifier::string),
                }),
                FormatFragment::Literal(String::from(" ")),
                FormatFragment::FormatSpecifier(FormatSpecifier {
                    options: FormatOptions {
                        index: Some(1),
                        flags: Default::default(),
                        width: Default::default(),
                        precision: Default::default(),
                        arg_size: None,
                    },
                    arg_type: SpecifierType::Builtin(ConversionSpecifier::string),
                }),
                FormatFragment::Literal(String::from(" ${* ?}\"\n")),
            ],
        };
        assert_eq!(
            actual, expected,
            "Expected left: {:#?}\n to equal right: {:#?}",
            actual, expected
        );
    }

    #[test]
    fn test_evaluate() {
        let input = "%i %*d %.*f %c %s";

        let parser = FormatStringParser::default();
        let input: FormatStringState = parser.state(input);

        let (leftover, result) = match FormatStringResult::parse(input) {
            Ok(f) => f,
            Err(e) => panic!("Could not parse format string '{}': {}", input.input, e),
        };
        assert_eq!(leftover.input, "");
        let mut actual = String::new();

        // This is actually wrong becuase %*d and %.*f both should have args inserted before their corresponding arg in this
        // And also - if you do it for one - you have to do it for all of them I believe...
        let argv = ["0", "1", "2.2", "122", "forty two"]
            .into_iter()
            .map(|s| s.to_owned())
            .collect();

        let args = StringArgvProvider { offset: 0, argv };
        result.evaluate(&args, &mut actual).unwrap();

        let expected = "0 1 2.200000 z forty two";
        assert_eq!(actual, expected);
    }
}
