#!/usr/bin/env bash

set -e

die() {
    test -z "$1" || echo "$1" > /dev/stderr
    exit 1
}

which which > /dev/null || die "which not found"
which jq > /dev/null || die "jq not found"


#This was about all I needed - except it just handles everything double and I have no way to force float etc.
# Which is probably fine, but in my use case, it isn't and it changed my results

# json on the input
#{ "name", "fmt","expect",args: [] }

test_case() {
    local JSON="$1"

    local NAME=""
    local FMT=""
    local EXPECT=""
    local RESULT=""

    NAME="$(echo "$JSON" | jq '.name' -r)"
    FMT="$(echo "$JSON" | jq '.fmt' -r)"
    EXPECT="$(echo "$JSON" | jq '.expect' -r)"

    readarray -t ARGS < <(echo "$JSON" | jq -cbr '.args[]'   | sed -E '/^$/d')

    printf "%s:\t%s\t\t%s\t" "${NAME@Q}" "${FMT@Q}" "${EXPECT@Q}"

    printf -v RESULT "$FMT" "${ARGS[@]}"
    printf "%s\t[" "${RESULT@Q}"

    printf "'%s'," "${ARGS[@]}"
    printf "]\n"
}



jq -c '.cases[]' | (
    while read -r CASE; do
        test_case "$CASE"
    done
)


#'empty':        ''              ''      ''      ['',]
#'liter string': 'literal string'                'literal string'        'literal string'        ['',]
#'a % literal':  'a %% literal'          'a % literal'   'a % literal'   ['',]
#'decimal positive':     '%d'            '3'     '3'     ['3',]
#'decimal negative':     '%d'            '-3'    '-3'    ['-3',]
#'float positive':       '%f'            '3.0'   '3.000000'      ['3.0',]
#'float negative':       '%f'            '-3.0'  '-3.000000'     ['-3.0',]
#'string':       '%s'            'string'        'string'        ['string',]
#'char': '%c'            'z'     '3'     ['30',]
#'all types':    '%c %s %d %f'           '30 string 2600 42.42'  '3 string 2600 42.420000'       ['30','string','2600','42.42',]
