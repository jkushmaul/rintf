#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

void usage(const char *fmt, ...)
{

    va_list vargs;
    va_start(vargs, fmt);
    if (fmt)
    {
        vfprintf(stderr, fmt, vargs);
    }
    va_end(vargs);
    if (fmt)
    {
        printf("\n");
    }

    fprintf(stderr, "Usage: test_printf <name> <fmt> [type=value]]\n  Where type: [i32,u32,f32,f64,str,u8] and only one is supported\n");
    exit(1);
}

int main(int argc, char **argv)
{
    char *pEnd = NULL;

    if (argc != 3)
    {
        usage("Expected 4 parameters but got %d", argc);
    }
    char *fmt = argv[1];
    char *parm = argv[2];

    test_varadic(argc, argv);

    if (!strncmp(parm, "i32=", 4))
    {
        parm += 4;
        int v = strtol(parm, &pEnd, 10);
        if (parm == pEnd)
        {
            usage("Invalid i32: %s", parm);
        }
        printf(fmt, v);
    }
    else if (!strncmp(parm, "u32=", 4))
    {
        parm += 4;
        unsigned int v = strtoul(parm, &pEnd, 10);

        if (parm == pEnd)
        {
            usage("Invalid u32: %s", parm);
        }
        printf(fmt, v);
    }
    else if (!strncmp(parm, "f32=", 4))
    {
        parm += 4;
        float v = strtof(parm, &pEnd);
        if (parm == pEnd)
        {
            usage("Invalid f32: %s", parm);
        }
        printf(fmt, v);
    }
    else if (!strncmp(parm, "f64=", 4))
    {
        parm += 4;
        double v = strtod(parm, &pEnd);
        if (parm == pEnd)
        {
            usage("Invalid f64: %s", parm);
        }
        printf(fmt, v);
    }
    else if (!strncmp(parm, "u8=", 3))
    {
        parm += 3;
        unsigned int v = strtoul(parm, &pEnd, 10);
        if (parm == pEnd)
        {
            usage("Invalid u8 (pass numeric value not 'c'): %s", parm);
        }
        if (v > 0xff)
        {
            usage("Invalid char value");
        }
        printf(fmt, v);
    }
    else if (!strncmp(parm, "str=", 4))
    {
        parm += 4;
        printf(fmt, parm);
    }
    else
    {
        usage("Invalid type for param arg");
    }

    printf("\n");
}
