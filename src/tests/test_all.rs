use crate::{format_args::StringArgvProvider, format_parser::FormatStringParser};
use serde::Deserialize;

/*

Test assertions were a bit flawed.. it was testing f64 not f32.
So 24.3 -> 24.300000 and I could not get anything but 24.299999...
but it's because printf in shell, uses doubles not floats, and once that
is done, it does match.
*/

#[derive(Deserialize)]
struct TestCase {
    name: String,
    fmt: String,
    expect: String,
    args: Vec<String>,
}

#[derive(Deserialize)]
struct TestSuite {
    cases: Vec<TestCase>,
}

#[test]
pub fn test() {
    let test_suite = include_str!("test_cases.json");
    let test_suite: TestSuite = serde_json::from_str(test_suite).unwrap();

    let parser = FormatStringParser::default();
    for t in &test_suite.cases {
        if t.name == "zero flag, 5 width, float" {
            println!("{}", t.name);
        }
        t.assert(&parser);
    }
}

#[test]
pub fn test_int_zero_padding() {
    let fmt = "%15.5d";
    let expected = "          00024";
    let args = StringArgvProvider {
        offset: 0,
        argv: vec![String::from("24")],
    };
    let parser = FormatStringParser::default();
    let result = parser.parse(fmt).unwrap();
    let mut out = String::new();
    result.evaluate(&args, &mut out).unwrap();
    assert_eq!(out, expected);
}

#[test]
pub fn test_f32_zero_width_precision() {
    let fmt = "%15.5f";
    let expected = "       24.30000";
    let args = StringArgvProvider {
        offset: 0,
        argv: vec![String::from("24.3")],
    };
    let parser = FormatStringParser::default();
    let result = parser.parse(fmt).unwrap();
    let mut out = String::new();
    result.evaluate(&args, &mut out).unwrap();
    assert_eq!(out, expected);
}

impl TestCase {
    fn assert(&self, parser: &FormatStringParser) {
        let result = match parser.parse(&self.fmt) {
            Ok(r) => r,
            Err(e) => panic!(
                "Test[{}] faield to parse'{}':\n {:#?}",
                self.name, self.fmt, e
            ),
        };

        let mut actual = String::new();
        let args = StringArgvProvider {
            offset: 0,
            argv: self.args.to_vec(),
        };

        match result.evaluate(&args, &mut actual) {
            Ok(r) => r,
            Err(e) => panic!(
                "Test[{}] failed to evaluate '{}':\n{:#?}",
                self.name, self.fmt, e
            ),
        };

        assert_eq!(
            actual, self.expect,
            "Test[{}] expected '{}' to become '{}' but got '{}'",
            self.name, self.fmt, self.expect, actual
        );
    }
}
