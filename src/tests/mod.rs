mod test_all;

pub fn assert_list_equals<T: std::fmt::Debug + PartialEq>(
    test_case: &str,
    actual: &[T],
    expected: &[T],
) {
    for i in 0..actual.len().min(expected.len()) {
        let actual = format!("{:#?}", actual[i]);
        let expected = format!("{:#?}", expected[i]);
        if actual != expected {
            let mut diff_index = 0;
            for i in 0..actual.len().min(expected.len()) {
                if expected.as_bytes()[i] == actual.as_bytes()[i] {
                    diff_index = i;
                    continue;
                }
                break;
            }
            let same = &actual[0..diff_index];
            let diff = &actual[diff_index..];

            panic!(
                    "Case[{}]: Element[{}] not equal!\nActual: length {}\nExpected: length {}\n****************\nSame up to [{}]:\n{}\n****************\nDiff:\n {}",
                    test_case,
                    i,
                    actual.len(),
                    expected.len(),
                    diff_index, same, diff
                );
        }
    }

    if actual.len() < expected.len() {
        panic!(
            "Case[{}]: Actual length {} < expected length {}",
            test_case,
            actual.len(),
            expected.len()
        );
    } else if actual.len() > expected.len() {
        panic!(
            "Case[{}]: Actual length {} > expected length {}",
            test_case,
            actual.len(),
            expected.len()
        );
    }
}
