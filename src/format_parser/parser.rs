use super::FormatStringState;
use crate::{error::PrintfError, format_string::FormatStringResult};
use nom::{
    branch::alt,
    bytes::complete::{is_not, tag},
    character::complete::digit1,
    combinator::{map, map_res},
    IResult, InputLength,
};

/// Each component in format string implements this which provides ::parse()
pub trait FormatStringType {
    fn parse<'a>(input: FormatStringState<'a>) -> IResult<FormatStringState<'a>, Self>
    where
        Self: Sized;
}

/// Contains configuration for format string parsing
#[derive(Default, Debug, PartialEq)]
pub struct FormatStringParser {
    extensions: String,
}

impl FormatStringParser {
    pub(crate) fn state<'a>(&'a self, input: &'a str) -> FormatStringState<'a> {
        FormatStringState {
            input,
            extensions: &self.extensions,
        }
    }
    pub fn parse<'a>(&'a self, format_str: &'a str) -> Result<FormatStringResult, PrintfError> {
        let state = self.state(format_str);

        let (leftover, format_string) = FormatStringResult::parse(state)
            .map_err(|e| PrintfError::ParseError(format!("{}", e)))?;

        if leftover.input_len() > 0 {
            return Err(PrintfError::TrailingFormatting(
                leftover.input.to_owned(),
                format_str.to_owned(),
            ));
        }

        Ok(format_string)
    }

    pub fn with_extension(mut self, code: char) -> Result<Self, PrintfError> {
        if self.extensions.contains(code) {
            return Err(PrintfError::DuplicateExtensionCode(code));
        }
        self.extensions.push(code);
        Ok(self)
    }
}

fn parse_string_literal_escaped_percent<'a>(
    input: FormatStringState<'a>,
) -> IResult<FormatStringState<'a>, &str> {
    map(tag("%%"), |_| "%")(input)
}

fn parse_string_literal_no_percent<'a>(
    input: FormatStringState<'a>,
) -> IResult<FormatStringState<'a>, &str> {
    map(is_not("%"), |s: FormatStringState<'a>| s.input)(input)
}

pub fn parse_string_literal<'a>(
    input: FormatStringState<'a>,
) -> IResult<FormatStringState<'a>, &str> {
    alt((
        parse_string_literal_no_percent,
        parse_string_literal_escaped_percent,
    ))(input)
}

pub fn parse_usize<'a>(input: FormatStringState<'a>) -> IResult<FormatStringState<'a>, usize> {
    map_res(digit1, |input: FormatStringState<'a>| {
        let i = input.input;

        let res = usize::from_str_radix(i, 10);
        res
    })(input)
}

#[cfg(test)]
pub mod test {
    use super::FormatStringParser;
    use crate::{
        format_options::FormatOptions,
        format_parser::{
            parse_string_literal, parse_usize, parser::parse_string_literal_escaped_percent,
        },
        format_specifier::{ConversionSpecifier, FormatSpecifier, SpecifierType},
        format_string::{FormatFragment, FormatStringResult},
    };

    #[test]
    fn test_parse_usize() {
        let input = "123";
        //  let arg_types = vec![];
        let parser = FormatStringParser::default();
        let state = parser.state(input);

        let (leftover, actual) = parse_usize(state).unwrap();
        let expected: usize = 123;
        assert_eq!(actual, expected);
        assert_eq!(leftover.input, "");
    }

    #[test]
    fn test_parse_string_literal_escaped_percent() {
        let input = "%% percent";

        let parser = FormatStringParser::default();
        let state = parser.state(input);

        let (leftover, actual) = parse_string_literal_escaped_percent(state).unwrap();
        assert_eq!(leftover.input, " percent");

        let expected = "%";
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_parse_string_literal() {
        let input = "%% percent";

        let parser = FormatStringParser::default();
        let state = parser.state(input);

        let (leftover, actual) = parse_string_literal(state).unwrap();
        let expected = "%";
        assert_eq!(actual, expected);
        assert_eq!(leftover.input, " percent");

        let (leftover, actual) = parse_string_literal(leftover).unwrap();
        let expected = " percent";
        assert_eq!(actual, expected);
        assert_eq!(leftover.input, "");
    }
    #[test]
    fn test_parse() {
        let input = "I wasted %d hours of my life on this";
        let actual = FormatStringParser::default().parse(input).unwrap();
        let expected = FormatStringResult {
            fragments: vec![
                FormatFragment::Literal("I wasted ".into()),
                FormatFragment::FormatSpecifier(FormatSpecifier {
                    options: FormatOptions::default(),
                    arg_type: SpecifierType::Builtin(ConversionSpecifier::decimalI32_d),
                }),
                FormatFragment::Literal(" hours of my life on this".into()),
            ],
        };
        assert_eq!(actual, expected);
    }
}
