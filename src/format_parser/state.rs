use nom::{Compare, InputIter, InputLength, InputTake, Slice, UnspecializedInput};
use std::{
    ops::RangeFrom,
    str::{CharIndices, Chars},
};

/// The input + state for format string parsing
/// Allows transferring configuration data through the parsing, such as, format extensions...
#[derive(Debug, Clone, Copy)]
pub struct FormatStringState<'a> {
    //pub arg_types: &'a [ArgTypeExtension],
    pub input: &'a str,
    pub extensions: &'a str,
}

impl<'a> InputLength for FormatStringState<'a> {
    fn input_len(&self) -> usize {
        self.input.len()
    }
}
impl<'a> InputTake for FormatStringState<'a> {
    fn take(&self, count: usize) -> Self {
        let input = self.input.take(count);

        Self {
            input,
            extensions: self.extensions,
        }
    }

    fn take_split(&self, count: usize) -> (Self, Self) {
        //  let arg_types = self.arg_types;
        let (input_a, input_b) = self.input.take_split(count);
        let a = Self {
            input: input_a,
            extensions: self.extensions,
        };
        let b = Self {
            input: input_b,
            extensions: self.extensions,
        };

        (a, b)
    }
}

impl<'a> Compare<&str> for FormatStringState<'a> {
    fn compare(&self, t: &str) -> nom::CompareResult {
        self.input.compare(t)
    }

    fn compare_no_case(&self, t: &str) -> nom::CompareResult {
        self.input.compare_no_case(t)
    }
}

impl<'a> InputIter for FormatStringState<'a> {
    type Item = char;
    type Iter = CharIndices<'a>;
    type IterElem = Chars<'a>;

    fn iter_indices(&self) -> Self::Iter {
        self.input.iter_indices()
    }

    fn iter_elements(&self) -> Self::IterElem {
        self.input.iter_elements()
    }

    fn position<P>(&self, predicate: P) -> Option<usize>
    where
        P: Fn(Self::Item) -> bool,
    {
        self.input.position(predicate)
    }

    fn slice_index(&self, count: usize) -> Result<usize, nom::Needed> {
        self.input.slice_index(count)
    }
}

impl<'a> UnspecializedInput for FormatStringState<'a> {}

impl<'a> Slice<RangeFrom<usize>> for FormatStringState<'a> {
    fn slice(&self, range: RangeFrom<usize>) -> Self {
        Self {
            input: self.input.slice(range),
            extensions: self.extensions,
        }
    }
}
