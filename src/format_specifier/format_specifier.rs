use super::ConversionSpecifier;
use crate::{
    format_options::FormatOptions,
    format_parser::{FormatStringState, FormatStringType},
};
use nom::{branch::alt, bytes::complete::tag, character::streaming::one_of, IResult};
use std::fmt::{Display, Formatter, Write};

/// A combination of format options - common across all types
/// And the actual type, a conversion specifier

#[derive(Clone, Debug, PartialEq)]
pub enum SpecifierType {
    Builtin(ConversionSpecifier),
    Extension(char),
}

#[derive(Clone, Debug, PartialEq)]
pub struct FormatSpecifier {
    pub options: FormatOptions,
    pub arg_type: SpecifierType,
}

impl Display for FormatSpecifier {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("%")?;
        if let Some(index) = self.options.index {
            f.write_fmt(format_args!("{}$", index))?;
        }

        self.options.flags.fmt(f)?;

        if let Some(width) = self.options.width.get() {
            f.write_fmt(format_args!("{}", width))?;
        }
        if let Some(precision) = self.options.precision.get() {
            f.write_fmt(format_args!("{}", precision))?;
        }

        if let Some(arg_size) = self.options.arg_size {
            f.write_fmt(format_args!("{}", arg_size))?;
        }

        match self.arg_type {
            SpecifierType::Builtin(b) => {
                return f.write_fmt(format_args!("{}", b));
            }
            SpecifierType::Extension(c) => {
                return f.write_char(c);
            }
        }
    }
}

impl FormatStringType for FormatSpecifier {
    fn parse<'a>(input: FormatStringState<'a>) -> IResult<FormatStringState<'a>, Self>
    where
        Self: Sized,
    {
        let (input, _) = tag("%")(input)?;
        let (input, options) = FormatOptions::parse(input)?;

        let (input, arg_type) = alt((format_specifier_extension, format_specifier_builtin))(input)?;

        let specifier = FormatSpecifier { options, arg_type };

        Ok((input, specifier))
    }
}

fn format_specifier_builtin<'a>(
    input: FormatStringState<'a>,
) -> IResult<FormatStringState<'a>, SpecifierType> {
    let (input, c) = ConversionSpecifier::parse(input)?;
    let c = SpecifierType::Builtin(c);
    Ok((input, c))
}
fn format_specifier_extension<'a>(
    input: FormatStringState<'a>,
) -> IResult<FormatStringState<'a>, SpecifierType> {
    let (input, c) = one_of(input.extensions)(input)?;
    let x = SpecifierType::Extension(c);
    return Ok((input, x));
}

#[cfg(test)]
pub mod test {
    use super::FormatSpecifier;
    use crate::format_parser::{FormatStringParser, FormatStringState, FormatStringType};

    #[test]
    fn test_format_specifier_leftover() {
        let input = "%d is%but this is not";
        let parser = FormatStringParser::default();
        let input: FormatStringState = parser.state(input);

        let (leftover, _) = FormatSpecifier::parse(input).unwrap();

        assert_eq!(leftover.input, " is%but this is not");
    }
}
