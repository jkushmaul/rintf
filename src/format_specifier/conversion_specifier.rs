use crate::{
    error::PrintfError,
    format_args::ExpectedArgType,
    format_parser::{FormatStringState, FormatStringType},
};
use nom::{bytes::complete::take, combinator::map_res};
use std::{
    fmt::{Display, Formatter},
    str::FromStr,
};
use strum::{EnumIter, FromRepr};

/// Conversion specifiers
///       A character that specifies the type of conversion to be applied.
#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Copy, FromRepr, PartialEq, EnumIter)]
#[repr(u8)]
pub enum ConversionSpecifier {
    /// f32 in hex
    lowerHexF32 = b'a',
    /// f32 in HEX
    upperHexF32 = b'A',
    /// char
    char = b'c',
    /// i32
    decimalI32_d = b'd',
    /// f32
    scientificLower = b'e',
    /// f32
    scientificUpper = b'E',
    /// f32
    float = b'f',
    /// f32 shortest of %e or %f
    shortestLower = b'g',
    /// f32 shortest of %e or %f
    shortestUpper = b'G',
    /// i32
    decimalI32_i = b'i',
    /// u32 Octal
    octalU32 = b'o',
    /// void*
    pointer = b'p',
    /// &str
    string = b's',
    /// u32
    decimalUint32 = b'u',
    /// u32 hex
    lowerHexU32 = b'x',
    /// u32 HEX
    upperHexU32 = b'X',
}

impl FromStr for ConversionSpecifier {
    type Err = strum::ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() != 1 {
            return Err(strum::ParseError::VariantNotFound);
        }
        if let Some(s) = s.as_bytes().first().and_then(|c| Self::from_repr(*c)) {
            return Ok(s);
        }
        return Err(strum::ParseError::VariantNotFound);
    }
}

impl Display for ConversionSpecifier {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let s = self.get_identifier();
        return f.write_str(s);
    }
}
impl ConversionSpecifier {
    fn get_identifier(&self) -> &str {
        match self {
            ConversionSpecifier::lowerHexF32 => "a",
            ConversionSpecifier::upperHexF32 => "A",
            ConversionSpecifier::char => "c",
            ConversionSpecifier::decimalI32_d => "d",
            ConversionSpecifier::scientificLower => "e",
            ConversionSpecifier::scientificUpper => "E",
            ConversionSpecifier::float => "f",
            ConversionSpecifier::shortestLower => "g",
            ConversionSpecifier::shortestUpper => "G",
            ConversionSpecifier::decimalI32_i => "i",
            ConversionSpecifier::octalU32 => "o",
            ConversionSpecifier::pointer => "p",
            ConversionSpecifier::string => "s",
            ConversionSpecifier::decimalUint32 => "u",
            ConversionSpecifier::lowerHexU32 => "x",
            ConversionSpecifier::upperHexU32 => "X",
        }
    }
}

impl TryInto<ExpectedArgType> for ConversionSpecifier {
    type Error = PrintfError;

    fn try_into(self) -> Result<ExpectedArgType, Self::Error> {
        let t = match self {
            ConversionSpecifier::lowerHexF32
            | ConversionSpecifier::upperHexF32
            | ConversionSpecifier::scientificLower
            | ConversionSpecifier::scientificUpper
            | ConversionSpecifier::float
            | ConversionSpecifier::shortestLower
            | ConversionSpecifier::shortestUpper => ExpectedArgType::F32,

            ConversionSpecifier::char => ExpectedArgType::Char,

            ConversionSpecifier::decimalI32_d | ConversionSpecifier::decimalI32_i => {
                ExpectedArgType::I32
            }

            ConversionSpecifier::decimalUint32
            | ConversionSpecifier::lowerHexU32
            | ConversionSpecifier::upperHexU32
            | ConversionSpecifier::octalU32 => ExpectedArgType::U32,

            ConversionSpecifier::string => ExpectedArgType::String,

            ConversionSpecifier::pointer => {
                return Err(PrintfError::NotSupported("pointers".into()))
            }
        };

        Ok(t)
    }
}

impl FormatStringType for ConversionSpecifier {
    fn parse<'a>(
        input: crate::format_parser::FormatStringState<'a>,
    ) -> nom::IResult<crate::format_parser::FormatStringState<'a>, Self>
    where
        Self: Sized,
    {
        let (input, arg_type) = map_res(take(1 as usize), |a: FormatStringState<'a>| {
            ConversionSpecifier::from_str(a.input)
        })(input)?;

        Ok((input, arg_type))
    }
}
