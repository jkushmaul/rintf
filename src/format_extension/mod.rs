//! I need a way to register single letter type codes (printf spec is single character)
//! That lets one add extra conversionspecifiers - and also customized argument retreival
//! for that type (or it uses a builtin type)
//!
//! Examples might be a string_id as int argument, that is fetched out of an array
//! Or a Vector where the vector is the argument - and it has 3 parts to it that expand into several %fs
//! This is similar to rust's Display trait I suppose.
//! struct Vector{}
//! impl Display for Vector {
//!    fn fmt() {
//!      fmt.write(format_args!{"{} {} {}", self.x, self.y, self.z})
//!    }
//! }

#[cfg(test)]
mod tests {

    use crate::{
        error::PrintfError,
        format_args::{
            branch_formatter::{int_format::IntFormatter, BranchFormatter},
            FormatArgProvider, StringArgvProvider,
        },
        format_options::FormatOptions,
        format_parser::FormatStringParser,
        format_specifier::{FormatSpecifier, SpecifierType},
        format_string::{FormatFragment, FormatStringResult},
    };

    struct VectorStringArgvProvider {
        inner: StringArgvProvider,
    }
    impl FormatArgProvider for VectorStringArgvProvider {
        type E = PrintfError;

        fn get_argc_offset(&self) -> usize {
            self.inner.get_argc_offset()
        }

        fn arg_u8(&self, arg_index: usize) -> Result<u8, Self::E> {
            self.inner.arg_u8(arg_index)
        }

        fn arg_i8(&self, arg_index: usize) -> Result<i8, Self::E> {
            self.inner.arg_i8(arg_index)
        }

        fn arg_i16(&self, arg_index: usize) -> Result<i16, Self::E> {
            self.inner.arg_i16(arg_index)
        }

        fn arg_i32(&self, arg_index: usize) -> Result<i32, Self::E> {
            self.inner.arg_i32(arg_index)
        }

        fn arg_u32(&self, arg_index: usize) -> Result<u32, Self::E> {
            self.inner.arg_u32(arg_index)
        }

        fn arg_f32(&self, arg_index: usize) -> Result<f32, Self::E> {
            self.inner.arg_f32(arg_index)
        }

        fn arg_str(&self, arg_index: usize) -> Result<&str, Self::E> {
            self.inner.arg_str(arg_index)
        }

        fn format<W: std::fmt::Write>(
            &self,
            extension: char,
            options: &FormatOptions,
            arg_index: usize,
            fmt: &mut W,
        ) -> Result<(), Self::E> {
            if extension != 'v' {
                return Err(PrintfError::ArgumentError(format!(
                    "Extensions are not supported: {}",
                    extension
                )))?;
            }

            let sarg = self.inner.arg_str(arg_index)?;
            let mut v = [0.0f32; 3];
            let s: Vec<&str> = sarg.split(" ").collect();
            if s.len() != v.len() {
                return Err(PrintfError::ArgumentError(format!(
                    "Invalid vector format: {}",
                    sarg
                )))?;
            }

            for i in 0..v.len() {
                v[i] = s[i].parse().map_err(|e| {
                    PrintfError::ArgumentError(format!("Invalid float value: {}: {}", s[i], e))
                })?;
            }
            let formatter = IntFormatter::default();

            formatter.branch_fmt(options, fmt, v[0] as i32)?;
            fmt.write_str(" ")?;
            formatter.branch_fmt(options, fmt, v[1] as i32)?;
            fmt.write_str(" ")?;
            formatter.branch_fmt(options, fmt, v[2] as i32)?;

            Ok(())
        }
    }

    #[test]
    fn test_vector_extension_parsed() {
        let fmt = "%v";
        let vector_extension = 'v';
        let parser = FormatStringParser::default()
            .with_extension(vector_extension)
            .unwrap();

        let result = parser.parse(fmt).unwrap();
        let arg_type = SpecifierType::Extension(vector_extension);
        let s = FormatSpecifier {
            options: FormatOptions::default(),
            arg_type,
        };
        let expected = vec![FormatFragment::FormatSpecifier(s)];

        assert_eq!(result.fragments, expected);
    }

    #[test]
    fn test_vector_extension_evaluated() {
        let vector_extension = 'v';

        let x = SpecifierType::Extension(vector_extension);
        let x = FormatSpecifier {
            options: FormatOptions::default(),
            arg_type: x,
        };

        let fragments = vec![FormatFragment::FormatSpecifier(x)];
        let result = FormatStringResult { fragments };

        let args = StringArgvProvider {
            offset: 0,
            argv: vec!["1 2 3".into(), "4".into(), "5".into()],
        };
        let args = VectorStringArgvProvider { inner: args };

        let mut actual = String::new();
        result.evaluate(&args, &mut actual).unwrap();

        let expected = "1 2 3";
        assert_eq!(actual, expected);
    }

    /*


        // I'm not in love with the placement of this...
        #[test]
        fn test_vector_extension() {
            let fmt = "%v";
            let parser = FormatStringParser::default().with_extension(ArgTypeExtension {
                type_match: String::from("v"),
                expand: |_, opts| {
                    let f = FormatFragment::FormatSpecifier(FormatSpecifier {
                        options: opts.clone(),
                        arg_type: ConversionSpecifier::float,
                    });
                    let space = FormatFragment::Literal(String::from(" "));
                    Ok(vec![
                        FormatFragment::Literal(String::from(", ")),
                        f.clone(),
                        space.clone(),
                        f.clone(),
                        space,
                        f,
                    ])
                },
            });
            let format_string = match parser.parse(&fmt) {
                Ok(f) => f,
                Err(e) => panic!("Could not parse format string '{}': {}", fmt, e),
            };
            let expected = FormatStringResult {
                fragments: vec![
                    FormatFragment::Literal(String::from(", ")),
                    FormatFragment::FormatSpecifier(FormatSpecifier {
                        options: FormatOptions::default(),
                        arg_type: ConversionSpecifier::float,
                    }),
                    FormatFragment::Literal(String::from(" ")),
                    FormatFragment::FormatSpecifier(FormatSpecifier {
                        options: FormatOptions::default(),
                        arg_type: ConversionSpecifier::float,
                    }),
                    FormatFragment::Literal(String::from(" ")),
                    FormatFragment::FormatSpecifier(FormatSpecifier {
                        options: FormatOptions::default(),
                        arg_type: ConversionSpecifier::float,
                    }),
                ],
            };
            assert_eq!(format_string, expected);
        }

    */
}
