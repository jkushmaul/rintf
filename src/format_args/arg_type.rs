use super::{ArgValue, FormatArgProvider};
use crate::format_options::FormatOptions;

/// This is the rust type that is expected from the provider
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum ExpectedArgType {
    Char,
    I8,
    I16,
    U32,
    I32,
    F32,
    String,
}

impl ExpectedArgType {
    pub(crate) fn get_arg<'a, A: FormatArgProvider>(
        &self,
        options: &FormatOptions,
        argc_offset: usize,
        args: &'a A,
    ) -> Result<ArgValue<'a>, A::E> {
        let thisarg = options.index.unwrap_or(argc_offset);
        //let use_default = thisarg >= argc;
        //let thisarg = Constants::OFS_PARM0 + 3 * thisarg as usize;

        // I don't know how I will handle these
        //      crate::ArgFormatFlags::Space  -> blank for the missing "+" on a positive string, or spaces for string
        //      crate::ArgFormatFlags::AlternateForm
        // Tough break for internatials... so sorry.
        //      crate::ArgFormatFlags::Glibc => todo!(),
        //      crate::ArgFormatFlags::Thousands

        let arg = match self {
            ExpectedArgType::Char => {
                let b: u8 = args.arg_u8(thisarg)?;
                ArgValue::from(b as char)
            }
            ExpectedArgType::I8 => {
                let b: i8 = args.arg_i8(thisarg)?;
                ArgValue::from(b)
            }
            ExpectedArgType::I16 => {
                let s: i16 = args.arg_i16(thisarg)?;
                ArgValue::from(s)
            }
            ExpectedArgType::U32 => {
                let u: u32 = args.arg_u32(thisarg)?;
                ArgValue::from(u)
            }
            ExpectedArgType::I32 => {
                let i: i32 = args.arg_i32(thisarg)?;
                ArgValue::from(i)
            }
            ExpectedArgType::F32 => {
                let f: f32 = args.arg_f32(thisarg)?;
                ArgValue::from(f)
            }
            ExpectedArgType::String => {
                let s: &str = args.arg_str(thisarg)?;
                ArgValue::from(s)
            }
        };

        Ok(arg)
    }
}
