use crate::{error::PrintfError, format_options::FormatOptions};
use std::{error::Error, fmt::Write};

use super::branch_formatter::{
    float_format::FloatFormatter, int_format::IntFormatter, string_format::StringFormatter,
    BranchFormatter,
};

pub trait FormatArgProvider {
    type E: Error + From<PrintfError> + From<std::fmt::Error>;

    // If the format string itself was an arg
    //  then it will be one.
    // If it is just a trivial list of arguments to formatstring
    //  then it will be zero.
    fn get_argc_offset(&self) -> usize;

    fn arg_u8(&self, arg_index: usize) -> Result<u8, Self::E>;
    fn arg_i8(&self, arg_index: usize) -> Result<i8, Self::E>;
    fn arg_i16(&self, arg_index: usize) -> Result<i16, Self::E>;
    fn arg_i32(&self, arg_index: usize) -> Result<i32, Self::E>;
    fn arg_u32(&self, arg_index: usize) -> Result<u32, Self::E>;
    fn arg_f32(&self, arg_index: usize) -> Result<f32, Self::E>;
    fn arg_str(&self, arg_index: usize) -> Result<&str, Self::E>;

    fn format<W: std::fmt::Write>(
        &self,
        extension: char,
        options: &FormatOptions,
        arg_index: usize,
        fmt: &mut W,
    ) -> Result<(), Self::E>;
}

pub enum ArgValue<'a> {
    Char(char),
    I8(i8),
    I16(i16),
    I32(i32),
    U32(u32),
    F32(f32),
    String(&'a str),
}

impl<'a> From<char> for ArgValue<'a> {
    fn from(value: char) -> Self {
        Self::Char(value)
    }
}
impl<'a> From<i8> for ArgValue<'a> {
    fn from(value: i8) -> Self {
        Self::I8(value)
    }
}
impl<'a> From<i16> for ArgValue<'a> {
    fn from(value: i16) -> Self {
        Self::I16(value)
    }
}
impl<'a> From<i32> for ArgValue<'a> {
    fn from(value: i32) -> Self {
        Self::I32(value)
    }
}
impl<'a> From<u32> for ArgValue<'a> {
    fn from(value: u32) -> Self {
        Self::U32(value)
    }
}
impl<'a> From<f32> for ArgValue<'a> {
    fn from(value: f32) -> Self {
        Self::F32(value)
    }
}
impl<'a> From<&'a str> for ArgValue<'a> {
    fn from(value: &'a str) -> Self {
        Self::String(value)
    }
}

impl<'a> ArgValue<'a> {
    pub fn format<W: Write>(
        &self,
        options: &FormatOptions,
        fmt: &mut W,
    ) -> Result<(), std::fmt::Error> {
        match self {
            ArgValue::Char(arg) => {
                let arg = arg.to_string();
                let formatter = StringFormatter;
                return formatter.branch_fmt(options, fmt, &arg);
            }
            ArgValue::I8(arg) => {
                let formatter = IntFormatter;
                return formatter.branch_fmt(options, fmt, *arg);
            }
            ArgValue::I16(arg) => {
                let formatter = IntFormatter;
                return formatter.branch_fmt(options, fmt, *arg);
            }
            ArgValue::I32(arg) => {
                let formatter = IntFormatter;
                return formatter.branch_fmt(options, fmt, *arg);
            }
            ArgValue::U32(arg) => {
                let formatter = IntFormatter;
                return formatter.branch_fmt(options, fmt, *arg);
            }
            ArgValue::F32(arg) => {
                let formatter = FloatFormatter::default();
                return formatter.branch_fmt(options, fmt, *arg);
            }
            ArgValue::String(arg) => {
                let formatter = StringFormatter;
                return formatter.branch_fmt(options, fmt, &arg);
            }
        }
    }
}

pub struct StringArgvProvider {
    pub offset: usize,
    pub argv: Vec<String>,
}
impl StringArgvProvider {
    fn get_arg(&self, index: usize) -> Result<&str, PrintfError> {
        if index < self.offset {
            return Err(PrintfError::ArgumentError(format!(
                "{} is under offset",
                index
            )));
        }

        match self.argv.get(index) {
            Some(s) => Ok(s),
            None => Err(PrintfError::ArgumentError(format!("{} is invalid", index))),
        }
    }
}
impl FormatArgProvider for StringArgvProvider {
    type E = PrintfError;

    fn arg_i8(&self, arg_index: usize) -> Result<i8, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn arg_i16(&self, arg_index: usize) -> Result<i16, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn arg_i32(&self, arg_index: usize) -> Result<i32, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn arg_u32(&self, arg_index: usize) -> Result<u32, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn arg_f32(&self, arg_index: usize) -> Result<f32, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn arg_str(&self, arg_index: usize) -> Result<&str, Self::E> {
        self.get_arg(arg_index)
    }

    fn arg_u8(&self, arg_index: usize) -> Result<u8, Self::E> {
        let s = self.get_arg(arg_index)?;
        Ok(s.parse()?)
    }

    fn get_argc_offset(&self) -> usize {
        self.offset
    }

    fn format<W: std::fmt::Write>(
        &self,
        extension: char,
        _options: &FormatOptions,
        _argc_offset: usize,
        _fmt: &mut W,
    ) -> Result<(), Self::E> {
        return Err(PrintfError::ArgumentError(format!(
            "Extensions are not supported: {}",
            extension
        )))?;
    }
}
