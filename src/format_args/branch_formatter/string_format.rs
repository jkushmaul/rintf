use super::BranchFormatter;
use crate::format_options::{ArgFormatFlag, FormatOptions};
use std::fmt::Write;

pub struct StringFormatter;
impl BranchFormatter<&str> for StringFormatter {
    fn get_flags(&self, options: &FormatOptions) -> flagset::FlagSet<ArgFormatFlag> {
        let flags = options.flags.flags - ArgFormatFlag::Zero - ArgFormatFlag::MarkPositive;
        flags
    }
    fn left_space<W: Write>(
        &self,
        fmt: &mut W,
        arg: &str,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let arg = if arg.len() == 0 { " " } else { arg };
        if precision > 0 {
            self.left_space_with_precision(fmt, arg, width, precision)
        } else {
            self.left_space_without_precision(fmt, arg, width)
        }
    }
    fn right_space<W: Write>(
        &self,
        fmt: &mut W,
        arg: &str,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let arg = if arg.len() == 0 { " " } else { arg };
        if precision > 0 {
            self.right_space_with_precision(fmt, arg, width, precision)
        } else {
            self.right_space_without_precision(fmt, arg, width)
        }
    }
}
