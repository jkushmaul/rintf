pub mod float_format;
pub mod int_format;
pub mod string_format;

use crate::format_options::{ArgFormatFlag, FormatOptions};
use flagset::FlagSet;
use std::fmt::{Display, Write};

/*
Peculiarities:
 * Default f32 precision
    * C default precision is 6 so 3.0 prints as 3.000000
    * Rust default precision is none, and behaves more like "as much as needed" so it prints as '3' (no .0,etc.)
 * Sign padding
    * C ' ' flag - for numerics only, ignored non numeric, pads a space for + if positive, but does not print it.
    * Rust has no concept.
 * Int zero padding for precision
    * '%15.5d'
        * In C this results as expected,   '          00024' with 5 padded zeros and the rest of the 15 width
        * In rust, the padding is completely ignored  "'{:>width$.precision$}'" -> '             24'
           *OR* you padd ALL of the width  "{:0>width$.precision$}" -> '000000000000024'

*/
pub trait BranchFormatter<T: Display> {
    fn get_precision(&self, options: &FormatOptions) -> usize {
        options.precision.get().unwrap_or_default()
    }
    fn get_width(&self, options: &FormatOptions) -> usize {
        options.width.get().unwrap_or_default()
    }
    fn get_flags(&self, options: &FormatOptions) -> FlagSet<ArgFormatFlag> {
        options.flags.flags
    }

    fn right_plus_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!(
            "{:>+width$.precision$}",
            arg,
            width = width,
            precision = precision
        ))
    }

    fn right_plus_without_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!("{:>+width$}", arg, width = width))
    }

    fn right_plus<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        if precision > 0 {
            self.right_plus_with_precision(fmt, arg, width, precision)
        } else {
            self.right_plus_without_precision(fmt, arg, width)
        }
    }
    fn right_zero_plus_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!(
            "{0:>+width$.precision$}",
            arg,
            width = width,
            precision = precision
        ))
    }
    fn right_zero_plus_without_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!("{:0>+width$}", arg, width = width))
    }

    fn right_zero_plus<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        if precision > 0 {
            self.right_zero_plus_with_precision(fmt, arg, width, precision)
        } else {
            self.right_zero_plus_without_precision(fmt, arg, width)
        }
    }

    fn right_zero_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!(
            "{:0>width$.precision$}",
            arg,
            width = width,
            precision = precision
        ))
    }
    fn right_zero_without_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!("{:0>width$}", arg, width = width))
    }
    fn right_zero<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        if precision > 0 {
            self.right_zero_with_precision(fmt, arg, width, precision)
        } else {
            self.right_zero_without_precision(fmt, arg, width)
        }
    }

    fn right_zero_space_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!(
            "{: >width$.precision$}",
            arg,
            width = width,
            precision = precision
        ))
    }
    fn right_zero_space_without_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!("{: >width$}", arg, width = width))
    }
    fn right_zero_space<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        if precision > 0 {
            self.right_zero_space_with_precision(fmt, arg, width, precision)
        } else {
            self.right_zero_space_without_precision(fmt, arg, width)
        }
    }

    fn right_space_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!(
            "{: >width$.precision$}",
            arg,
            width = width,
            precision = precision
        ))
    }
    fn right_space_without_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!("{: >width$}", arg, width = width))
    }
    fn right_space<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        if precision > 0 {
            self.right_space_with_precision(fmt, arg, width, precision)
        } else {
            self.right_space_without_precision(fmt, arg, width)
        }
    }

    fn right_noflags_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!(
            "{:>width$.precision$}",
            arg,
            width = width,
            precision = precision
        ))
    }
    fn right_noflags_without_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!("{:>width$}", arg, width = width))
    }
    fn right_noflags<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        if precision > 0 {
            self.right_noflags_with_precision(fmt, arg, width, precision)
        } else {
            self.right_noflags_without_precision(fmt, arg, width)
        }
    }

    ////////////// The mutants, Lefties

    fn left_plus_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!(
            "{:<+width$.precision$}",
            arg,
            width = width,
            precision = precision
        ))
    }
    fn left_plus_without_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!("{:<+width$}", arg, width = width))
    }

    fn left_plus<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        if precision > 0 {
            self.left_plus_with_precision(fmt, arg, width, precision)
        } else {
            self.left_plus_without_precision(fmt, arg, width)
        }
    }

    fn left_only_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!(
            "{:<width$.precision$}",
            arg,
            width = width,
            precision = precision
        ))
    }

    fn left_only_without_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!("{:<width$}", arg, width = width))
    }

    fn left_only<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        if precision > 0 {
            self.left_only_with_precision(fmt, arg, width, precision)
        } else {
            self.left_only_without_precision(fmt, arg, width)
        }
    }

    fn left_space_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!(
            "{: <width$.precision$}",
            arg,
            width = width,
            precision = precision
        ))
    }

    fn left_space_without_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
    ) -> Result<(), std::fmt::Error> {
        fmt.write_fmt(format_args!("{: <width$}", arg, width = width))
    }

    fn left_space<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        if precision > 0 {
            self.left_space_with_precision(fmt, arg, width, precision)
        } else {
            self.left_space_without_precision(fmt, arg, width)
        }
    }

    fn branch_fmt<W: Write>(
        &self,
        options: &FormatOptions,
        fmt: &mut W,
        arg: T,
    ) -> Result<(), std::fmt::Error> {
        // Tough break for using all the options parsed...
        // format_spec := [[fill]align][sign]['#']['0'][width]['.' precision]type
        let precision = self.get_precision(options);
        let width = self.get_width(options);
        let flags = self.get_flags(options);

        // Consider all flags '%0 +-'
        if flags.contains(ArgFormatFlag::LeftAlign) {
            // * - ignores 0
            //'% +'
            if flags.contains(ArgFormatFlag::MarkPositive) {
                // * '+' ignores ' '
                return self.left_plus(fmt, arg, width, precision);
            }

            //' '
            if flags.contains(ArgFormatFlag::Space) {
                return self.left_space(fmt, arg, width, precision);
            }

            return self.left_only(fmt, arg, width, precision);
        }

        // Consider flags '%+0 '
        if flags.contains(ArgFormatFlag::MarkPositive) {
            // * '+' ignores ' '
            //'%0'
            if flags.contains(ArgFormatFlag::Zero) {
                // * '+' ignores ' '

                return self.right_zero_plus(fmt, arg, width, precision);
            }

            return self.right_plus(fmt, arg, width, precision);
        }

        // Consider flags '%0 '
        if flags.contains(ArgFormatFlag::Space) {
            if flags.contains(ArgFormatFlag::Zero) {
                // '%0 '
                return self.right_zero_space(fmt, arg, width, precision);
            }

            // '% '
            return self.right_space(fmt, arg, width, precision);
        }

        //'%0'
        if flags.contains(ArgFormatFlag::Zero) {
            return self.right_zero(fmt, arg, width, precision);
        }

        // No flags
        return self.right_noflags(fmt, arg, width, precision);
    }
}
