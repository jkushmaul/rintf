use super::BranchFormatter;
use crate::format_options::FormatOptions;
use std::fmt::Write;

pub struct FloatFormatter {
    default_precision: usize,
}
impl Default for FloatFormatter {
    fn default() -> Self {
        //C has a default precision of 6
        Self {
            default_precision: 6,
        }
    }
}

impl FloatFormatter {
    pub fn with_default_precision(default_precision: usize) -> Self {
        Self { default_precision }
    }
}

impl BranchFormatter<f32> for FloatFormatter {
    fn get_precision(&self, options: &FormatOptions) -> usize {
        options.precision.get().unwrap_or(self.default_precision)
    }

    fn left_space<W: Write>(
        &self,
        fmt: &mut W,
        arg: f32,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let mut s = String::new();
        self.left_plus(&mut s, arg, width, precision)?;
        let s = s.replace("+", " ");
        return fmt.write_str(&s);
    }
    fn right_space<W: Write>(
        &self,
        fmt: &mut W,
        arg: f32,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let mut s = String::new();
        self.right_plus(&mut s, arg, width, precision)?;
        let s = s.replace("+", " ");
        return fmt.write_str(&s);
    }
    fn right_zero_space<W: Write>(
        &self,
        fmt: &mut W,
        arg: f32,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let mut s = String::new();
        self.right_zero_plus(&mut s, arg, width, precision)?;
        let s = s.replace("+", " ");
        return fmt.write_str(&s);
    }
}
