use super::BranchFormatter;
use std::fmt::{Display, Write};

pub trait Integeral: Display {}

impl Integeral for i32 {}
impl Integeral for u32 {}
impl Integeral for i8 {}
impl Integeral for i16 {}
impl Integeral for u16 {}

#[derive(Default)]
pub struct IntFormatter;

impl<T: Integeral> BranchFormatter<T> for IntFormatter {
    fn right_plus_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let s = format!("{:>+.precision$}", arg, precision = precision);
        return fmt.write_fmt(format_args!("{:>width$}", s, width = width));
    }

    fn right_zero_plus_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let s = format!("{:0>+.precision$}", arg, precision = precision);
        return fmt.write_fmt(format_args!("{:>width$}", s, width = width));
    }

    fn right_zero_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let s = format!("{:0>.precision$}", arg, precision = precision);
        return fmt.write_fmt(format_args!("{:>width$}", s, width = width));
    }

    fn right_zero_space_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let mut s = String::new();
        self.right_zero_plus_with_precision(&mut s, arg, 0, precision)?;
        let s = s.replace("+", " ");
        return fmt.write_fmt(format_args!("{:>width$}", s, width = width));
    }

    fn right_space_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let mut s = String::new();
        self.right_zero_plus_with_precision(&mut s, arg, 0, precision)?;
        let s = s.replace("+", " ");
        return fmt.write_fmt(format_args!("{:>width$}", s, width = width));
    }

    fn right_noflags_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let s = format!("{:0>width$}", arg, width = precision);
        return fmt.write_fmt(format_args!("{:>width$}", s, width = width));
    }

    fn left_plus_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let s = format!("{:<+.precision$}", arg, precision = precision);
        return fmt.write_fmt(format_args!("{:<width$}", s, width = width));
    }

    fn left_space_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let s = format!("{:0>+.precision$}", arg, precision = precision);
        let s = s.replace("+", " ");
        return fmt.write_fmt(format_args!("{:>+width$}", s, width = width));
    }

    fn left_only_with_precision<W: Write>(
        &self,
        fmt: &mut W,
        arg: T,
        width: usize,
        precision: usize,
    ) -> Result<(), std::fmt::Error> {
        let s = format!("{:<.precision$}", arg, precision = precision);
        return fmt.write_fmt(format_args!("{:<width$}", s, width = width));
    }
}
