use std::num::{ParseFloatError, ParseIntError};

use nom::error::{ErrorKind, ParseError};
use thiserror::Error;

#[derive(Debug, Error, PartialEq)]
pub enum PrintfError {
    #[error("If any format specifiers provide parameter field, all must: {0}")]
    ParameterFieldRequiresAll(String),
    #[error("Parse Error: {0}")]
    ParseError(String),
    #[error(transparent)]
    Strum(#[from] strum::ParseError),
    #[error("Format string trailing chars: '{0}'  from '{1}'")]
    TrailingFormatting(String, String),
    #[error("{0} not supported yet, so sorry")]
    NotSupported(String),
    #[error(transparent)]
    FormatError(#[from] std::fmt::Error),
    #[error("Argument Error {0}")]
    ArgumentError(String),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error(transparent)]
    ParseFloatError(#[from] ParseFloatError),
    #[error("An extension already exists with code '{0}'")]
    DuplicateExtensionCode(char),
}

#[derive(Debug, Error)]
pub enum NomParsingError<I> {
    #[error("Positions must start from very beginning of argument list: {0}")]
    PositionsNotAllowed(usize),
    #[error("Positions once started must be carried through to the end: {0}")]
    ContinuedPositionsRequired(usize),
    #[error("Nom parsing error")]
    Nom(I, ErrorKind),
}

impl<I> ParseError<I> for NomParsingError<I> {
    fn from_error_kind(input: I, kind: ErrorKind) -> Self {
        NomParsingError::Nom(input, kind)
    }

    fn append(_: I, _: ErrorKind, other: Self) -> Self {
        other
    }
}
