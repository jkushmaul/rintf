# Rintf

Not the best Standard C library style formatted output conversion *parsing in rust

Format string documentation here: <https://www.man7.org/linux/man-pages/man3/printf.3.html>
And <https://www.gnu.org/software/libc/manual/html_node/Formatted-Output.html>

## Why

I didn't need a library to help me format a string instead of the superior rust approach etc.

I needed to parse the string.  No one exposed their parsing.  So I wrote my own.

## Usage

First, create a parser

```rust
let parser = FormatStringParser::default()
```

Parse

```rust
let input = "I wasted %d hours of my life on this";
let format_string: FormatStringResult = parser.parse(input).unwrap();
```

Now what?

`FormatStringResult` contains a list of fragments one can use to draw out the arguments

Because this isn't meant to be a downgrade in string formatting, it's meant to allow interop with C style format strings, so an actual API...
The actual formatting has to be done a bit differently due to variance of arguments and requires an "ArgProvider"

```rust
 impl FormatArgProvider for TestArgProvider {
    ...
        fn arg_u32(&self, arg_index: usize) -> Result<u32, crate::PrintfError> {
            let _ = self.validate_argc(arg_index)?;
            Ok(420_000)
        }

        fn arg_f32(&self, arg_index: usize) -> Result<f32, crate::PrintfError> {
            let _ = self.validate_argc(arg_index)?;
            Ok(42.42)
        }

        fn arg_str(&self, arg_index: usize) -> Result<&str, crate::PrintfError> {
            let _ = self.validate_argc(arg_index)?;
            Ok("forty two")
        }
 }

  let mut writer = String::new();
  let args = TestArgProvider { argc: 10 }; // Assuming this returns the args in example
  let result = format_string.evaluate(&args, &mut writer).unwrap();
  let expected = "420000 420000 42.42 -42 forty two";
  assert_eq!(writer, expected);
```

It is up to the arg provider to validate arg_index against it's own argc and error - or use a default.
Rintf will validate the format string and it's use of flags, but the actual bounds checks on the arguments
themselves have to be done in the arg provider.

## ConversionSpecifiers

Your basic set of types:

* `%d`
* `%f`
* ...

## Extensions

But you can create extensions to the parser - which uses all existing parsing
but allows you to expand to one or more (or none) builtins.

eg) `%v` vector - output 3 %f separated by spaces:

```rust
fn expand_vector_specifier(match_str: &str, options: &FormatOptions) -> Result<Vec<FormatSpecifier>> {
     let f = FormatFragment::FormatSpecifier(FormatSpecifier {
        options: opts.clone(),
        arg_type: ConversionSpecifier::float,
    });
    let space = FormatFragment::Literal(String::from(" "));
    Ok(vec![
        f.clone(),
        space.clone(),
        f.clone(),
        space,
        f,
    ])
}

let vec_extension = FormatExtension {
    type_match: String::from("v"),
    expand: expand_vector_specifier,
};

let parser = FormatStringParser::default().with_extension(vec_extension);
let mut writer = String::new();
let args = TestArgProvider { argc: 10 }; // Assuming this returns the args in example
let format_string = parser.parse("%v").unwrap();
let result = format_string.evaluate(&args, &mut writer).unwrap();
let expected = "42.42 42.42 42.42";
assert_eq!(writer, expected);

```

## "3 months later..."

Low and behold:  <https://docs.rs/sprintf/latest/sprintf/parser/index.html> exposes parser
it's all I ever wanted. But I made my own.

Some differences:

0. Quality
   I'm sure that one is better in terms of quality but I've done my best to ensure parity with C
   by generating format strings, arg lists and capturing libc output for each.

1. Argument Provider
   Rather than be left with the format fragments both APIs provide (The parse result),
   This project provides a mechanism for providing arguments when evaluating
   format fragments.
   This is highly volatile right now and may change signatures

2. FormatExtension
   Have fun with the family by adding arg type extensions which are just
   alternate forms of the builtin %d, %f, %s - %v for instance to expand into (%d, %d, %d) etc.
   `%v` - once it is used, it's used.

## Todo

* Extension identification

  Tight spaces - for non standard conversion specifiers, use a encapsulation character pair,
  such as `{}` or `[]`, and then extensions use a string to match vs a single character:

  eg)

    ```rust

        let parser = FormatParser::default().with_extension("vector"); //Pseudo code, there'd be a fn for what to do with vector when it is seen for evaluation.
        let result = parser.parse("%{vector}")?;
    ```

* Override conversion specifiers

  ```rust
    fn format_float(out: &mut W, options: &FormatOptions, float: f32) {
        //change default precision to 1
        let mut options = options.clone();
        options.precision = options.precision.unwrap_or(1);
        FloatFormatter.branch-fmt(out, &options, float);
    }
    let parser = FormatParser::default().override_specifier(ConversionSpecifier::float, override_float)?;
    let result = parser.parse("%f");
    let args = Args(24.123);
    let mut out = String::new();
    result.evaluate(out, args);
    assert_eq!(out, "24.3")
  ```

* Simplify standard interface

  This is what all the other libraries probably do better:
  > Just let me use a printf style format string, with compile time args

  eg)

   ```rust
   let s = rintf::sprintf(rintf_args!("%d %f %s", intVal, floatVal, stringVal));
   ```

  These means that the macro is going to have to parse the format string, determine args ahead of time
  and then use the provided args in a custom arg provider

  Possible Caveat: This won't work with extensions since the arguments aren't known at compile time.

* Arg Provider

  * Clean up `branch_fmt()`

  A function for every permutation sucks but I don't know what I can do about that.
  Macros, probably but that might be harder to read than individual functions

## thoughts

Not meant to be a TODO, just random thoughts

* Format Providers

  I have "StringFormatter" and "IntFormatter" and "FloatFormatter".
  I feel like these could also be "extensions".
  These are more how you'd override a conversion specifier too.. This is where the formatting actually takes place.

  So maybe for every extension, or conversion specifier, you have to also provide a formatter object.

  eg) Without thinking it through too much:

    Obviously this is incorrect and incomplete but something like this

    ```rust
    struct MyFloatFormatter;
    impl ArgFormatter for MyFloatFormatter {
        fn get_precision(&self, options: &FormatOptions) -> usize {
            // Provide a default 1 precision if none provided
            options.unwrap_or(1);
        }
    }

    let float_override = ConversionSpecifierOverride<AF: ArgFormatter> {
        specifier: ConversionSpecifier::float,
        formatter: MyFloatFormatter,
    };


    let vector_extension = FormatExtension {
        extension: "vector"
        arg: VectorFormatter
    }

    let parser = FormatParser::default().with_override(float_override).with_extension(vector_extension);

    let format_string = "%v";
    let result = parser.parse(format_string);
    let mut out = String::new();
    let args = Args(&["24.1234", "42.5678", "64.9012"])
    parser.evaluate(out, args);
    assert_eq!(out, "[24.1, 42.5, 64.9]");

    ```

I was trying to keep FormatParser separate from arg providers which then, also need to then have separate "Formatters".

Because, technically, the parser doesn't need to know about types of arguments...  It's the evaluation that is the problem usually.

Perhaps that can be done still by adding one more layer "Engine"

```rust
struct FormatEngine {
    formatters: Vec<ArgFormatter>,
    overrides: Vec<ConversionOverride>,
    extensions: Vec<Extensions>
    args: ArgProvider,
}
```

The parser is never even used directly then.

```rust
let format_engine = FormatEngine::default().add_override(float_override).add_extension(vector_extension).with_args(args);

let format_string = format_engine.parse("%{vector}")?;
let mut out = String::new();
format_engine.evaluate(&mut out, format_string)?;

assert_eq!(out, "[24.1, 42.5, 64.9]");


```
